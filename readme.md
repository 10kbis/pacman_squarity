# How to use

Go to [Squarity](https://squarity.pythonanywhere.com) and fill the tileset url with [https://gitlab.com/10kbis/pacman_squarity/-/raw/master/tileset.png](https://gitlab.com/10kbis/pacman_squarity/-/raw/master/tileset.png), the tileset data with the content of the tileset.json file and the game code with the main.py file.
